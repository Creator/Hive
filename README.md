###Collaborators are welcome/wanted


Hive is/will be a turtle based automation system. The user can assign jobs to different turtle types, each job can consist of multiple tasks. When a turtle finishes one job, it will request another, if none are available then it will go home.

You may have noticed that the github repo is in the form of a MC resource pack. This is to allow easy testing, just download to a minecraft resource folder.

Want to help? Fork the project, make some edits, and make a pull request. If you consistently give me code that I like, I'll make you a contributor (no more pull requesting) (Note to self: add collaborators here: https://github.com/lupus590/Hive/settings/collaboration)

The license is included in LICENSE.md
this project makes extensive use of other peoples code, these have there own licenses. The license deepest in the folder tree is the one that is "active" I.E. if you want to know how a file is licensed, navigate to the file and keep going up directories until you find a file named LICENSE. That file is the license for all of the files within its sibling folders. If you find another going deeper down the file tree, then that 'new' license *overrides* the previous.
